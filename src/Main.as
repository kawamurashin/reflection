package 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class Main extends Sprite 
	{
		private var bar:Array;
		private var ball:Array;
		private var dragPoint:Sprite;
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			layout();
		}
		
		private function layout():void 
		{
			var i:uint;
			var n:uint;
			var point:Sprite;
			var g:Graphics;
			var theta:Number;
			var radius:Number;
			bar = [];
			
			n = 2;
			for (i = 0; i < n; i++)
			{
				point = new Sprite();
				addChild(point);
				g = point.graphics;
				g.beginFill(0xFF0000);
				g.drawCircle(0, 0, 10);
				point.x = 20 + (stage.stageWidth - 40) * Math.random();
				point.y = 20 + (stage.stageHeight - 40) * Math.random();
				point.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
				bar.push(point);
				
			};
			
			ball = [];
			point = new Sprite();
			addChild(point);
			g = point.graphics;
			g.beginFill(0xCCCCCC);
			g.drawCircle(0, 0, 13);
			point.x = 20 + (stage.stageWidth - 40) * Math.random();
			point.y = 20 + (stage.stageHeight - 40) * Math.random();
			point.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			ball.push(point);
			
			radius = 50;
			theta = 2 * Math.PI * Math.random();
			
			point = new Sprite();
			addChild(point);
			g = point.graphics;
			g.beginFill(0xDDDDDD);
			g.drawCircle(0, 0, 5);
			point.x = ball[0].x + radius * Math.cos(theta);
			point.y = ball[0].y + radius * Math.sin(theta);

			point.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			ball.push(point);
			
			draw();
			
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		private function enterFrameHandler(e:Event):void 
		{
			draw();
		}
		
		private function mouseDownHandler(e:MouseEvent):void 
		{
			dragPoint = Sprite(e.currentTarget);
			dragPoint.startDrag();
			stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
		}
		
		private function mouseUpHandler(e:MouseEvent):void 
		{
			dragPoint.stopDrag();
			stage.removeEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
		}
		private function draw():void
		{
			var i:uint;
			var n:uint;
			var point:Sprite;
			var g:Graphics;
			//
			var abx:Number;
			var aby:Number;
			var nx:Number;
			var ny:Number;
			var length:Number;
			//
			var doc:Number
			//
			var dx:Number;
			var dy:Number;
			var d:Number;
			var t:Number;
			var cx:Number;
			var cy:Number;
			var dTheta:Number;
			var theta:Number;
			
			var a1:Number;
			var a2:Number;
			var b1:Number;
			var b2:Number;
			//
			n = bar.length;
			g = this.graphics;
			g.clear();
			g.lineStyle(2, 0xCCCCCC);
			g.moveTo(bar[0].x, bar[0].y);
			g.lineTo(bar[1].x, bar[1].y);
			g.endFill();
			//
			g.lineStyle(1, 0xCCCCCC);
			g.moveTo(ball[0].x, ball[0].y);
			g.lineTo(ball[1].x, ball[1].y);
			g.endFill();
			//
			abx = bar[1].x - bar[0].x;
			aby = bar[1].y - bar[0].y;
			nx = -aby;
			ny = abx;
			//単位ベクトル
			length = Math.sqrt(Math.pow(nx, 2) + Math.pow(ny, 2));
			if(length > 0)	length = 1 / length;
			nx *= length;
			ny *= length;
			
			//ボールの方向ベクトル
			dx = ball[1].x - ball[0].x;
			dy = ball[1].y - ball[0].y;
			
			//内積？　マイナスは？
			d = -(bar[0].x * nx + bar[0].y * ny);
			t = -(nx * ball[0].x +ny * ball[0].y +d ) / (nx * dx + ny  * dy);
			
			cx = ball[0].x + dx * t;
			cy = ball[0].y + dy * t;
			
			//バーと左右判定　外積
			a1 = bar[1].x - bar[0].x;
			a2 = bar[1].y - bar[0].y;
			b1 = ball[0].x - bar[0].x;
			b2 = ball[0].y - bar[0].y;
			
			doc = (a1 * b2) - (a2 * b1);
			if (doc != 0)
			{
				doc = doc / Math.abs(doc);
			}
			nx *= doc;
			ny *= doc;
			
			//法線ベクトの左右判定　外積
			a1 = nx;
			a2 = ny;
			b1 = ball[0].x - cx;
			b2 = ball[0].y - cy;
			
			doc = (a1 * b2) - (a2 * b1);
			if (doc != 0)
			{
				doc = doc / Math.abs(doc);
			}
			//
			dTheta = Math.acos((a1 * b1 + a2 * b2) / ( Math.sqrt(Math.pow(a1, 2) + Math.pow(a2, 2) ) * Math.sqrt( Math.pow(b1, 2) + Math.pow(b2, 2))));
			theta = Math.atan2(ny, nx) - dTheta * doc;
			
			g.lineStyle(3, 0xCC0000);
			g.moveTo(cx, cy);
			g.lineTo(cx + nx*20, cy+ ny*20);
			g.endFill();
			
			g.beginFill(0xCC0000);
			g.drawCircle(cx, cy, 5);
			//反射線
			g.lineStyle(1, 0xCC0000);
			g.moveTo(cx, cy);
			g.lineTo(cx + 20 * Math.cos(theta) , cy+ 20 * Math.sin(theta));
			g.endFill();
			
			
		}
		
	}
	
}